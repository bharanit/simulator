import { Component, OnInit } from '@angular/core';
import * as echarts from 'echarts';
import { environment } from 'src/environments/environment.prod';
import { CommonService } from '../common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public oneTimeRequestParams: any;
  public allTimeRequestParams: any;
  public allTimeRandom: any;
  public oneTime: any = {
    "path": "https://dev.boodskap.io/api",
    "dkey": "XLOYLUDCHY",
    "akey": "7wqaskN4z31b",
    "rule": "message",
    "ruleName": "101",
  };
  public allTime: any = {
    "path": "https://dev.boodskap.io/api",
    "dkey": "XLOYLUDCHY",
    "akey": "7wqaskN4z31b",
    "rule": "message",
    "ruleName": "101",
    "time": 5000,
  };
  public showStopBtn = false;
  public setTimeInterval: any;
  constructor(public commonService: CommonService, private toastr: ToastrService) { }

  ngOnInit(): void {
    // this.chartInitialize(100)
    this.oneTimeRequestParams = JSON.stringify({
      "deviceid": "",
      "value": ""
    })
    this.allTimeRequestParams = JSON.stringify({
      "deviceid": ['GPS-1', 'GPS-2', 'GPS-3'],
      "value": [10, 20, 30]
    })
    this.allTimeRandom = JSON.stringify({
      "value": [10, 20, 30, 40, 50, 60, 70, 80]
    })
  }

  chartInitialize(averageScore) {
    setTimeout(() => {
      const ec = echarts as any;
      const container = document.getElementById('chart');
      const chart = ec.init(container);
      let option = {
        title: [{
          x: "50%",
          bottom: 0,
          text: 'Echarts',
          textStyle: {
            fontWeight: 'normal',
            fontSize: 15,
            color: "#6a6b70"
          },
        }],

        tooltip: {
          formatter: "{a} <br/>{b} : {c}%"
        },
        legend: {
          orient: 'vertical',
          left: 'bottom',
          data: ['%']
        },
        detail: { offsetCenter: [0, '50%'], color: '#6a6b70' },
        series: [
          {
            radius: '90%',
            name: 'Driver Overall Score',
            type: 'gauge',
            detail: { formatter: '{value}' },
            data: [{ value: averageScore, name: '%' }],
            axisLine: {
              show: true,
              lineStyle: {
                color: [
                  [0.4, '#c23531'],
                  [0.75, '#ffbf00'],
                  [0.835, '#4db26d'],
                  [1, '#4db26d']
                ],
                width: 20
              }
            },
          }
        ]

      };

      // setInterval(function () {
      //   option.series[0].data[0].value = (Math.random() * 100).toFixed(2) as any - 0;
      //   chart.setOption(option, true);
      // }, 2000);
      chart.setOption(option);
      //  this.getCurrentPosition(2000);
    }, 200);
  }

  oneTimePush() {
    if (this.oneTime) {
      if (this.oneTime.rule == "message") {
        const actionURL = this.oneTime.path + "/push/raw/" + this.oneTime.dkey + "/" + this.oneTime.akey + "/BOODSKAP/IOT/1.0/" + this.oneTime.ruleName + "?type=JSON";
        this.commonService.sendMessage(actionURL, this.oneTimeRequestParams)
          .subscribe(res => {
            this.toastr.success('Success!', JSON.stringify(res));
          }, err => {
            this.toastr.error('Error!', JSON.stringify(err));
          });
      } else if (this.oneTime.rule == "named") {
        const actionURL = this.oneTime.path + "/call/v2/execute/rule/" + this.oneTime.dkey + ":" + this.oneTime.akey;
        const sendObj = {
          "sessionId": this.commonService.guid(),
          "namedrule": this.oneTime.ruleName,
          "scriptArgs": JSON.parse(this.oneTimeRequestParams)
        };
        this.commonService.postCall(actionURL, JSON.stringify(sendObj))
          .subscribe(res => {
            this.toastr.success('Success!', JSON.stringify(res));
          }, err => {
            this.toastr.error('Error!', JSON.stringify(err));
          });
      } else if (this.oneTime.rule == "binary") {
        const actionURL = this.oneTime.path + "/push/bin/json/" + this.oneTime.dkey + "/" + this.oneTime.akey + "/BOODSKAP/IOT/1.0/" + this.oneTime.ruleName;
        this.commonService.postCall(actionURL, this.oneTimeRequestParams)
          .subscribe(res => {
            this.toastr.success('Success!', JSON.stringify(res));
          }, err => {
            this.toastr.error('Error!', JSON.stringify(err));
          });
      }
    } else {
      alert("Type required fields")
    }
  }

  allTimePush() {
    if (this.allTime.time) {
      try {
        let index = 0
        this.setTimeInterval = setInterval(() => {
          let getObj = JSON.parse(this.allTimeRequestParams)
          let sendObj ={}
          for (const iterator in getObj) {
            sendObj[iterator] = getObj[iterator][index] ? getObj[iterator][index]: getObj[iterator][0]
          }
          if (this.allTime.rule == "message") {
            const actionURL = this.allTime.path + "/push/raw/" + this.allTime.dkey + "/" + this.allTime.akey + "/BOODSKAP/IOT/1.0/" + this.allTime.ruleName + "?type=JSON";
            this.commonService.sendMessage(actionURL, JSON.stringify(sendObj))
              .subscribe(res => {
                this.toastr.success('Success!', 'Request='+ JSON.stringify(sendObj) +"Response="+JSON.stringify(res));
              }, err => {
                this.toastr.error('Error!', JSON.stringify(err));
              });
          } else if (this.allTime.rule == "named") {
            const actionURL = this.allTime.path + "/call/v2/execute/rule/" + this.allTime.dkey + ":" + this.allTime.akey;
            const setObj = {
              "sessionId": this.commonService.guid(),
              "namedrule": this.allTime.ruleName,
              "scriptArgs": sendObj
            };
            this.commonService.postCall(actionURL, JSON.stringify(setObj))
              .subscribe(res => {
                this.toastr.success('Success!', 'Request='+ JSON.stringify(setObj) +"Response="+JSON.stringify(res));
              }, err => {
                this.toastr.error('Error!', JSON.stringify(err));
              });
          } else if (this.allTime.rule == "binary") {
            const actionURL = this.allTime.path + "/push/bin/json/" + this.allTime.dkey + "/" + this.allTime.akey + "/BOODSKAP/IOT/1.0/" + this.allTime.ruleName;
            this.commonService.postCall(actionURL, JSON.stringify(sendObj))
              .subscribe(res => {
                this.toastr.success('Success!', 'Request='+ JSON.stringify(sendObj) +"Response="+JSON.stringify(res));
              }, err => {
                this.toastr.error('Error!', JSON.stringify(err));
              });
          }
          index += 1
        }, this.allTime.time);
      } catch (e) {
        this.toastr.error('Error!', JSON.stringify(e));
        this.showStopBtn = false;
        clearInterval(this.setTimeInterval)
      }
    } else {
      this.showStopBtn = false;
      this.toastr.warning('Type time interval in milli-seconds!');
    }
  }

  stopAllTimePush() {
    this.showStopBtn = false;
    clearInterval(this.setTimeInterval)
  }
}
