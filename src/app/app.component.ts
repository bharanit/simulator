import { Component } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { CommonService } from './common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'angularDemo';
  public userObj: any;
  public contactsList: any;

  constructor(public commonService: CommonService) {
  }

}

