import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { CommonService } from '../common.service';
import { DatePipe } from '@angular/common'
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  public source: LocalDataSource;
  public tableColumn: any;
  public defaultRowPerPage = 10;
  public defaultSizeFrom = 0;
  public tableData: any;
  public contactsList: any;
  public pageNumber: any = 1;
  constructor(public commonService: CommonService, public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.getUser();
  }


  getUser() {
    let query = {
      query: {
        "bool": {
          "should": [],
        }
      },
      "from": this.defaultSizeFrom,
      "size": this.defaultRowPerPage
    }

    const sendData = {
      "specId": environment.VENDOR_TABLE,
      "type": "RECORD",
      "query": JSON.stringify(query)
    }
    const actionURL = 'elastic/search/query/' + environment.DOMAIN_KEY + ':' + environment.API_KEY;

    this.commonService.postCall(actionURL, JSON.stringify(sendData))
      .subscribe(res => {
        res = this.commonService.elasticQueryFormatter(res);
        res = res.data;
        if (res.recordsTotal > 0) {
          this.contactsList = res.data;
          res.data.length = res.recordsTotal
          ///// table initilazation
          this.tableColumn = {
            actions: {
              add: false,
              edit: false,
              position: 'right'
            },
            delete: {
              deleteButtonContent: '<i class="fa fa-trash"></i>'
            },
            pager: {
              page: this.pageNumber,
              perPage: this.defaultRowPerPage
            },
            columns: {
              vendor_nam: {
                title: 'Vendor Name',
                filter: false,
                width: "20%"
              },
              primary_email: {
                title: 'Email',
                filter: false,
                width: "20%"
              },
              fns_num: {
                title: 'FNS',
                filter: false,
                width: "20%"
              },
              public_photo: {
                title: 'Image',
                filter: false,
                type: 'html',
                valuePrepareFunction: (data) => {
                  if (data) {
                    return '<img width="50" height="50" src="' + environment.fileUrl + data + '"/>'
                  }
                  return '<div class="badge badge-danger">not found</div>';
                }
              },
              create_ts: {
                title: 'Created Time',
                filter: false,
                valuePrepareFunction: (date) => {
                  if (date) {
                    return this.datepipe.transform(date, 'yyyy-MM-dd');
                  }
                  return null;
                }
              },
              last_updt_ts: {
                title: 'Last Updated Time',
                filter: false,
                valuePrepareFunction: (date) => {
                  if (date) {
                    return this.datepipe.transform(date, 'yyyy-MM-dd');
                  }
                  return null;
                }
              }
            }
          };
          this.tableData = new LocalDataSource(res.data);

          this.tableData.onChanged().subscribe((change) => {
            if (change.action === 'page') {
              // this.tableData.setPaging(this.pageNumber, this.defaultRowPerPage);
              this.defaultSizeFrom = (change.paging.page - 1)
              if (this.defaultSizeFrom != 0) {
                this.defaultSizeFrom = this.defaultSizeFrom * change.paging.perPage
              }
              this.defaultRowPerPage = change.paging.perPage;
              this.pageNumber = change.paging.page
              this.getUser();
              this.tableData.setPaging(this.pageNumber, this.defaultRowPerPage);
              setTimeout(() => {
                this.tableData.setPaging(this.pageNumber, this.defaultRowPerPage);
              }, 500);
            }
          });


        } else {
          this.contactsList = [];
        }
      }, err => {
        console.log('Something went wrong! Please try again later')
      });
  }

  onSearch(query: string = '') {
    if (query != '') {
      this.tableData.setFilter([
        {
          field: 'vendor_nam',
          search: query
        },
        {
          field: 'primary_email',
          search: query
        },
        {
          field: 'fns_num',
          search: query
        }
      ], false);
    } else {
      this.tableData = new LocalDataSource(this.contactsList);
    }
  }

  setPager() {
    this.tableData.setPaging(1, Number(this.defaultRowPerPage));
  }
}
